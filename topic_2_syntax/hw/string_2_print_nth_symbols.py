def print_nth_symbols(my_str, n):
    """
    Функция print_nth_symbols.
    
    Принимает строку и натуральное число n (целое число > 0).
    Вывести символы с индексом n, n*2, n*3 и так далее.
    Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'
    
    Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
    Если тип n не int, то вывести строку 'Must be int!'.
    
    Если n больше длины строки, то вывести пустую строку.
    """
    if type(n) != int:
        return 'Must be int!'
    elif n <= 0:
        return 'Must be > 0!'
    elif n > len(my_str):
        return ''

    while n < len(my_str)-1:
        for i in range(len(my_str)):
            if i == n:
                return my_str[n]
    i += 1


if __name__ == "__main__":
    print (print_nth_symbols('abcdefghijklmnopqrstuvwxyz', 1))
