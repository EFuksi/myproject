def print_symbol(my_str):
    """
    Функция print_symbols_if.

    Принимает строку.

    Если строка нулевой длины, то вывести строку "Empty string!".

    Если длина строки больше 5, то вывести первые три символа и последние три символа.
    Пример: string='123456789' => result='123789'

    Иначе вывести первый символ столько раз, какова длина строки.
    Пример: string='345' => result='333'
    """

    if len(my_str) == 0:
        return "Empty string!"

    if len(my_str) > 5:
        return my_str[:3] + my_str[-3:]
    else:
        return my_str[1] * len(my_str)


if __name__ == "__main__":
    print (print_symbol('123456789'))


