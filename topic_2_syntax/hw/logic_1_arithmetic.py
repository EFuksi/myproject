def arithmetic(a, b, opr):
    """
    Функция arithmetic.

    Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
    Если третий аргумент +, сложить их;
    если —, то вычесть;
    если *, то умножить;
    если /, то разделить (первое на второе).
    В остальных случаях вернуть строку "Unknown operator".
    Вернуть результат операции.
    """
    if opr == '+':
        return (a+b)
    elif opr == '-':
        return (a-b)
    elif opr == '/':
        return (a/b)
    elif opr == '*':
        return (a*b)
    else:
        return ("Unknown operator")

if __name__ == "__main__":
    print (arithmetic(24, 15, '///'))

