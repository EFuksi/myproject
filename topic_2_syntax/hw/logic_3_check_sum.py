def check_sum(a, b, c):
    """
    Функция check_sum.

    Принимает 3 числа.
    Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
    """
    if a+b == c:
        return True
    if a+c == b:
        return True
    if b + c == a:
        return True
    else:
        return False


if __name__ == "__main__":
    print (check_sum(24, 15, 9))
