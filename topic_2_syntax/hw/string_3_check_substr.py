def check_substr(st_1, st_2):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """

    if len(st_1) == '' or len(st_2) == '':
        return False

    if len(st_1) == len(st_2):
        return False

    if len(st_1) > len(st_2) and st_2 in st_1:
        return True
    elif len(st_1) < len(st_2) and st_1 in st_2:
        return True
    else:
        return False


def main():
    print(check_substr('eeeabcdefeee', 'abycdef'))


if __name__ == '__main__':
    main()


