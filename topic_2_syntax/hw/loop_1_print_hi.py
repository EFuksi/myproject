def print_hi(n):
    """
    Функция print_hi.

    Принимает число n.
    Выведите на экран n раз фразу "Hi, friend!"
    Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
    """

    return n * 3


if __name__ == "__main__":
    print (print_hi("Hi, friend!"))
