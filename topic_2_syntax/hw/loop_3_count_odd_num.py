def count_odd_num(n):
    """
    Функция count_odd_num.

    Принимает натуральное число (целое число > 0).
    Верните количество нечетных цифр в этом числе.
    Если число равно 0, то вернуть "Must be > 0!".
    Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
    """

    if type(n) != int:
        return 'Must be int!'

    if n < 0:
        return None

    if n == 0:
        return 'Must be > 0!'

    acc = 0
    for i in str(n):
        if int(i) % 2 != 0:
            acc += 1
    return acc


if __name__ == "__main__":
    print(count_odd_num(136))
