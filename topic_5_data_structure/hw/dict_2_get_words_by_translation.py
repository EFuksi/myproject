def get_words_by_translation(my_dict, new_word):
    """
    Функция get_words_by_translation.

    Принимает 2 аргумента:
        ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
        слово для поиска в словаре (eng).

    Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
    если нет, то ‘Can’t find English word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """

    if type(my_dict) != dict:
        return 'Dictionary must be dict!'
    if len(my_dict) == 0:
        'Dictionary is empty!'
    if type(new_word) != str:
        return 'Word must be str!'
    if len(new_word) == 0:
        return 'Word is empty!'

    result_words = []
    for k, v in my_dict.items():
        if new_word in v:
            result_words.append(k)
    return result_words if len(result_words) > 0 else 'No word'


if __name__ == '__main__':
    print(get_words_by_translation({'спокойный': ['calm', 'quiet'], 'покинувший': ['left'], 'налево': ['left']},
                                   'left'))

