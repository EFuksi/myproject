def get_translation_by_word(my_dict, new_word):
    """
    Функция get_translation_by_word.

    Принимает 2 аргумента:
        ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
        слово для поиска в словаре (ru).

    Возвращает все варианты переводов (list), если такое слово есть в словаре,
    если нет, то ‘Can’t find Russian word: {word}’.

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
    Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
    """

    if type(my_dict) != dict:
        return 'Dictionary must be dict!'
    if len(my_dict) == 0:
        'Dictionary is empty!'
    if type(new_word) != str:
        return 'Word must be str!'
    if len(new_word) == 0:
        return 'Word is empty!'

    return my_dict.get(new_word, f'Can’t find Russian word: {new_word}')


if __name__ == '__main__':
    print(get_translation_by_word({'спокойный': ['calm', 'quiet'], 'счастливый': ['cheerful', 'happy']}, 'счастливый'))
