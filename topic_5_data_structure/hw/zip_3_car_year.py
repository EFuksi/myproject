from itertools import zip_longest


def zip_car_year(car_list, production_list):
    """
    Функция zip_car_year.

    Принимает 2 аргумента: список с машинами и список с годами производства.

    Возвращает список с парами значений из каждого аргумента, если один список больше другого,
    то заполнить недостающие элементы строкой “???”.

    Подсказка: zip_longest.

    Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
    Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
    """

    if type(car_list) != list or type(production_list) != list:
        return 'Must be list!'

    if len(car_list) == 0 or len(production_list) == 0:
        return 'Empty list!'

    return list(zip_longest(car_list, production_list, fillvalue='???'))


if __name__ == '__main__':
    print(zip_car_year(['bmw', 'toyota', 'mercedes'], ['1994', '2002', '2010', '2020']))

