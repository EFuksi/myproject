import itertools


def pow5_start_inf_step(start, step):
    """
    Функция pow5_start_inf_step.

    Принимает 2 аргумента: число start, step.

    Возвращает генератор-выражение состоящий из
    значений в 5 степени от start до бесконечности с шагом step.

    Пример: start=3, step=2 результат 3^5, 5^5, 7^5, 9^5 ... (infinity).

    Если start или step не являются int, то вернуть строку 'Start and Step must be int!'.
    """

    if type(start) != int or type(step) != int:
        return 'Start and Step must be int!'

    return (k ** 5 for k in itertools.count(star=start, step=step))


if __name__ == '__main__':
    my_gen = pow5_start_inf_step(2, 3)

    for i in range(25):
        print(next(my_gen))

