def dict_to_list(my_dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает кортеж: (
                список ключей,
                список значений,
                количество уникальных элементов в списке ключей,
                количество уникальных элементов в списке значений
                ).

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
    """

    if type(my_dict) != dict:
        return 'Must be dict!'

    dict_keys = list(my_dict.keys())
    dict_values = list(my_dict.values())
    set_keys = set(my_dict.keys())
    set_values = set(my_dict.values())
    num_unique_keys = len(set_keys)
    num_unique_values = len(set_values)

    result = (dict_keys, dict_values, num_unique_keys, num_unique_values)
    return result


if __name__ == '__main__':
    print(dict_to_list({1: 3, 2: 5, 3: 3}))

