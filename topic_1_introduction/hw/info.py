name = input("Введите имя: ")
surname = input("Введите фамилию: ")
age = input("Введите возраст: ")

print(f"Вас зовут {name} {surname}! Ваш возраст равен {age}.")
print()
print("Вас зовут ", name, " ", surname, "! Ваш возраст равен ", age, ".", sep='')
print()
print("Вас зовут " + name + " " + surname + "! Ваш возраст равен " + age + ".")