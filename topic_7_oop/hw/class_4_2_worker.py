class Worker:
    """
    Класс Worker.

    Поля:
        имя: name,
        зарплата: salary,
        должность: position.

    Методы:
        __gt__: возвращает результат сравнения (>) зарплат работников.
        __len__: возвращает количетсво букв в названии должности.
    """
    def __init__(self, name, salary, position):
        self.name = name
        self.salary = salary
        self.position = position

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)


if __name__ == '__main__':
    w1 = Worker('Nick', 2200, 'history teacher')
    w2 = Worker('Daniel', 4300, 'art teacher')
    print(w1 > w2)
    print(w1.__gt__(w2))
    print(w1.__len__())
    print(len(w2))
    print(w1.salary)




