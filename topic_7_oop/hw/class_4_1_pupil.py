from statistics import mean


class Pupil:
    """
    Класс Pupil.

    Поля:
        имя: name,
        возраст: age,
        dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

    Методы:
        get_all_marks: получить список всех оценок,
        get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
        get_avg_mark: получить средний балл (все предметы),
        __le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
    """

    def __init__(self, n, a, marks):
        self.name = n
        self.age = a
        self.marks = marks

    # def get_all_marks(self):
    #     result = []
    #     for k, v in self.marks.items():
    #         result.append(v)
    #     return result

    def get_all_marks(self):
        result = [v for sublist in self.marks.values() for v in sublist]
        return result

    # one more way? for get_all_marks ?

    def get_avg_mark_by_subject(self):
        result = {k: mean(v) for k, v in self.marks.items()}
        return result

    # one more way? for get_avg_mark ?

    def get_avg_mark(self):
        result = [v for sublist in self.marks.values() for v in sublist]
        return mean(result)

    def __le__(self, other):
        result1 = [v for sublist in self.marks.values() for v in sublist]
        result2 = [v for sublist in other.marks.values() for v in sublist]
        return result1 <= result2


if __name__ == "__main__":
    p1 = Pupil('Anton', 12, {'math': [3, 5], 'english': [5, 5, 4]})
    p2 = Pupil('Sasha', 12, {'math': [4, 5], 'english': [3, 5, 3]})
    print(p1.get_all_marks())
    print(p1.get_avg_mark_by_subject())
    print(p1.get_avg_mark())
    print(p1.__le__(p2))


