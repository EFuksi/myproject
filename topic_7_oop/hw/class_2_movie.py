class Movie:
    """
    Класс Movie.

    Поля:
        duration_min,
        name,
        year.

    При создании экземпляра инициализировать поля класса.

    Перегрузить оператор __str__, который возвращает строку вида
    "Наименование фильма: name | Год выпуска: year | Длительность (мин): duration_min".

    Перегрузить оператор __ge__, который
    возвращает True, если self.duration_min => other_duration_min, иначе False.
    """

    def __init__(self, dur, n, y):
        self.duration_min = dur
        self.name = n
        self.year = y

    def __str__(self):
        return "Наименование фильма: " + self.name + " | Год выпуска: " + str(self.year) + " | Длительность (мин): " + str(self.duration_min)

    def __ge__(self, other):
        return other.duration_min <= self.duration_min


if __name__ == '__main__':
    movie1 = Movie(90, 'Terminator', 2001)
    print(movie1)
    movie2 = Movie(17, 'Short movie', 2009)
    print(movie2 <= movie1)

