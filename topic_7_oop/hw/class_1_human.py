class Human:
    """
    Класс Human.
    
    Поля:
        age,
        first_name,
        last_name.
    
    При создании экземпляра инициализировать поля класса.
    
    Создать метод get_age, который возвращает возраст человека.
    
    Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.
    
    Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
    """

    def __init__(self, a, fn, ln):
        self.age = a
        self.first_name = fn
        self.last_name = ln

    def get_age(self):
        return self.age

    def __eq__(self, other):
        return self.age == other.age and self.first_name == other.first_name and self.last_name == other.last_name

    def __str__(self):
        return "Имя: " + self.first_name + " " + self.last_name + " Возраст: " + str(self.age)


if __name__ == '__main__':
    man1 = Human(36, 'Igor', 'Zaitsev')
    print(man1)
    print(man1.get_age())
    man2 = Human(36, 'Igor', 'Zaitsev')
    print((man1 == man2))



