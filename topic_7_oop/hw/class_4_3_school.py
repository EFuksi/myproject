from statistics import mean
from topic_7_oop.hw.class_4_1_pupil import Pupil
from topic_7_oop.hw.class_4_2_worker import Worker


class School:
    """
    Класс School.

    Поля:
        список людей в школе (общий list для Pupil и Worker): people,
        номер школы: number.

    Методы:
        get_avg_mark: вернуть средний балл всех учеников школы
        get_avg_salary: вернуть среднюю зп работников школы
        get_worker_count: вернуть сколько всего работников в школе
        get_pupil_count: вернуть сколько всего учеников в школе
        get_pupil_names: вернуть все имена учеников (с повторами, если есть)
        get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
        get_max_pupil_age: вернуть возраст самого старшего ученика
        get_min_worker_salary: вернуть самую маленькую зп работника
        get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
                                        (список из одного или нескольких элементов)
    """

    def __init__(self, sch_num):
        self.people = []
        self.school_number = sch_num

    def all_people(self, peop):
        return self.people.extend(peop)

    def get_avg_mark(self):
        result = []
        for p in self.people:
            if type(p) == Pupil:
                result += [v for sublist in p.marks.values() for v in sublist]
        return mean(result)

    # the below way does work :)
    # def get_avg_salary(self):
    #     result = []
    #     for w in self.people:
    #         if type(w) == Worker:
    #             result += [w.salary]
    #     return mean(result)

    def get_avg_salary(self):
        result = []
        for w in self.people:
            if type(w) == Worker:
                result.append(w.salary)
        return mean(result)

    def get_worker_count(self):
        w_counter = 0
        for w in self.people:
            if type(w) == Worker:
                w_counter += 1
        return w_counter

    def get_pupil_count(self):
        p_counter = 0
        for p in self.people:
            if type(p) == Pupil:
                p_counter += 1
        return p_counter

    def get_pupil_names(self):
        result = f'Pupil names:\n'
        for p in self.people:
            if type(p) == Pupil:
                result += f'\t{p.name}'
        return result

    def get_unique_worker_positions(self):
        result = []
        for w in self.people:
            if type(w) == Worker:
                result.append(w.position)
                unique_result = set(result)
        return unique_result

    def get_max_pupil_age(self):
        p_age_lst = []
        for p in self.people:
            if type(p) == Pupil:
                p_age_lst.append(p.age)
        return max(p_age_lst)

    def get_min_salary_worker_names(self):
        w_salary_lst = []
        for w in self.people:
            if type(w) == Worker:
                w_salary_lst.append(w.salary)
        return min(w_salary_lst)

    # how can I do same using the above fucntion?

    def get_min_salary_worker_names(self):
        w_salary_lst = []
        for w in self.people:
            if type(w) == Worker:
                w_salary_lst.append(w.salary)
                min_salary = min(w_salary_lst)
                w_names = []
                if w.salary == min_salary:
                    w_names.append(w.name)
                return w_names


if __name__ == '__main__':
    s1 = School(36)
    p1 = Pupil('Anton', 12, {'math': [3, 5], 'english': [5, 5, 4]})
    p2 = Pupil('Sasha', 15, {'math': [1, 2], 'english': [3, 5, 3]})
    w1 = Worker('Nick', 2200, 'history teacher')
    w2 = Worker('Daniel', 4300, 'art teacher')
    peop = [p1, p2, w1, w2]
    s1.all_people(peop)
    print(s1.people)
    print(s1.get_avg_mark())
    print(s1.get_worker_count())
    print(s1.get_pupil_count())
    print(s1.get_avg_salary())
    print(s1.get_pupil_names())
    print(s1.get_unique_worker_positions())
    print(s1.get_max_pupil_age())
    print(s1.get_min_salary_worker_names())


