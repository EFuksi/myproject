class Chicken:
    """
    Класс Chicken.

    Поля:
        имя: name,
        номер загона: corral_num,
        сколько яиц в день: eggs_per_day.

    Методы:
        get_sound: вернуть строку 'Ко-ко-ко',
        get_info: вернуть строку вида:
            "Имя курицы: name
             Номер загона: corral_num
             Количество яиц в день: eggs_per_day"
        __lt__: вернуть результат сравнения количества яиц (<)
    """

    def __init__(self, n, c_num, eggs_d):
        self.name = n
        self.corral_num = c_num
        self.eggs_per_day = eggs_d

    def get_sound(self):
        return 'Ко-ко-ко'

    def get_info(self):
        return f'Имя курицы: {self.name}\nНомер загона: {self.corral_num}\nКоличество яиц в день: {self.eggs_per_day}'

    def __lt__(self, other):
        return self.eggs_per_day == other.eggs_per_day

if __name__ == "__main__":
    chick1 = Chicken('Yulya', 1, 5)
    chick2 = Chicken('Zhanna', 2, 7)

    print(chick1.get_sound())
    print(chick2.get_sound())
    print('-----------------------------')
    print(chick1.get_info())
    print(chick2.get_info())
    print('-----------------------------')
    print(chick1 < chick2)










