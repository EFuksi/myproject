class Goat:
    """
    Класс Goat.

    Поля:
        имя: name,
        возраст: age,
        сколько молока дает в день: milk_per_day.

    Методы:
        get_sound: вернуть строку 'Бе-бе-бе',
        __invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
        __mul__: умножить milk_per_day на число. вернуть self
    """

    def __init__(self, n, a, milk):
        self.name = n
        self.age = a
        self.milk_per_day = milk

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        return self.name[::-1]

    def __mul__(self, n):
        return self.milk_per_day * n


if __name__== "__main__":
    g1 = Goat('Нюша', 5, 10)
    print(g1.get_sound())
    print(~g1)
    print(g1.__invert__())
    print(g1.__mul__(5))


