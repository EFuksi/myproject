from topic_7_oop.hw.class_3_1_chicken import Chicken
from topic_7_oop.hw.class_3_2_goat import Goat


class Farm:
    """
    Класс Farm.

    Поля:
        животные (list из произвольного количества Goat и Chicken): animals
                            (вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
        наименование фермы: name,
        имя владельца фермы: owner.

    Методы:
        get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
        get_chicken_count: вернуть количество куриц на ферме,
        get_animals_count: вернуть количество животных на ферме,
        get_milk_count: вернуть сколько молока можно получить в день,
        get_eggs_count: вернуть сколько яиц можно получить в день.
    """

    def __init__(self, n, o):
        self.animals = []
        self.name = n
        self.owner = o

    def add_animal(self, animal):
        self.animals.append(animal)

    def add_some_animals(self, animals):
        self.animals.extend(animals)

    def get_goat_count(self):
        goat_count = 0
        for g in self.animals:
            if type(g) == Goat:
                goat_count += 1
        return goat_count

    def get_chicken_count(self):
        chick_count = 0
        for c in self.animals:
            if type(c) == Chicken:
                chick_count += 1
        return chick_count

    def get_animals_count(self):
        animal_count = 0
        for a in self.animals:
            animal_count += 1
        return animal_count

    def get_eggs_count(self):
        count1 = 0
        for x in self.animals:
            if type(x) == Chicken:
                count1 += x.eggs_per_day
        return count1

    def get_milk_count(self):
        count2 = 0
        for x in self.animals:
            if type(x) == Goat:
                count2 += x.milk_per_day
        return count2


if __name__ == "__main__":
    f1 = Farm('Veselaya', 'Petya')
    chick1 = Chicken('Yulya', 1, 5)
    chick2 = Chicken('Zhanna', 2, 7)
    g1 = Goat('Нюша', 5, 10)
    g2 = Goat('Нюра', 2, 13)

    f1.animals.append(g1)
    f1.animals.append(g2)
    f1.animals.extend((chick1, chick2))
    print(f1.animals)
    print('--------------------')
    print(f1.get_goat_count())
    print(f1.get_chicken_count())
    print('--------------------')
    print(f1.get_animals_count())
    print('--------------------')
    print(f1.get_eggs_count())
    print(f1.get_milk_count())









